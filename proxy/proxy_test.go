package proxy_test // import "electric-it.io/cago/proxy"

import (
	"net"
	"net/http"
	"net/url"
	"os"
	"testing"

	"github.com/apex/log"
	"github.com/spf13/viper"

	"electric-it.io/cago/proxy"
)

const (
	FAKEPROXYURL_GOOD       = "http://127.0.0.1:45000"
	FAKEPROXYURL_UNPARSABLE = "THIS IS NOT A URL"

	FAKEENDPOINT_TCP  = "127.0.0.1:45000"
	FAKEENDPOINT_HTTP = "http://127.0.0.1:45000"
)

func init() {
	viper.Set("TESTMODE", true)

	os.Unsetenv("HTTP_PROXY")
	os.Unsetenv("http_proxy")

	log.SetLevel(log.DebugLevel)
}

func TestCanAccessTCPEndpoint_Sad_BlankURL(t *testing.T) {
	canAccessEndpoint := proxy.CanAccessTCPEndpoint("")

	if canAccessEndpoint {
		t.Fatalf("Expected to get error when passing in empty URL")
	}
}

func TestCanAccessTCPEndpoint_Sad_NoServer(t *testing.T) {
	canAccessEndpoint := proxy.CanAccessTCPEndpoint(FAKEENDPOINT_TCP)

	if canAccessEndpoint {
		t.Fatalf("Expected to get error when transport returns a 500 error")
	}
}

func TestCanAccessTCPEndpoint_Happy(t *testing.T) {
	listener, listenError := net.Listen("tcp", FAKEENDPOINT_TCP)
	if listenError != nil {
		t.Fatalf("Unable to start fake server for testing: %+v", listenError)
	}

	go func() {
		defer listener.Close()
		_, acceptError := listener.Accept()
		if acceptError != nil {
			t.Errorf("Unexpected error accepting the connection: %+v", acceptError)
		}

		t.Log("Connection accepted")
	}()

	canAccessEndpoint := proxy.CanAccessTCPEndpoint(FAKEENDPOINT_TCP)

	if !canAccessEndpoint {
		t.Fatalf("Expected to be able to access the endpoint")
	}
}

func TestCanAccessEndpoint_Happt_NoProxy(t *testing.T) {
	viper.Set("ignore-proxy-config", true)

	endpointURL, parseError := url.ParseRequestURI(FAKEENDPOINT_HTTP)
	if parseError != nil {
		t.Fatalf("Unable to parse URI: %+v", parseError)
	}

	proxyURL, _ := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyURL != nil {
		t.Fatalf("Expected no proxy to be set, instead it was %+v", proxyURL)
	}
}

func TestCanAccessEndpoint_HTTP_PROXY(t *testing.T) {
	os.Setenv("HTTP_PROXY", FAKEPROXYURL_GOOD)
	os.Unsetenv("http_proxy")
	viper.Set("ignore-proxy-config", false)

	endpointURL, parseError := url.ParseRequestURI(FAKEENDPOINT_HTTP)
	if parseError != nil {
		t.Fatalf("Unable to parse URI: %+v", parseError)
	}

	listener, listenError := net.Listen("tcp", FAKEENDPOINT_TCP)
	if listenError != nil {
		t.Fatalf("Unable to start fake server for testing: %+v", listenError)
	}

	go func() {
		defer listener.Close()
		_, acceptError := listener.Accept()
		if acceptError != nil {
			t.Errorf("Unexpected error accepting the connection: %+v", acceptError)
		}

		t.Log("Connection accepted")
	}()

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL == nil {
		t.Fatalf("Didn't expect proxy to be nil")
	}

	if proxyURL.String() != FAKEPROXYURL_GOOD {
		t.Fatalf("Expected proxy to be set to %+v, instead it was: %+v", FAKEPROXYURL_GOOD, proxyURL.String())
	}
}

func TestCanAccessEndpoint_http_proxy(t *testing.T) {
	os.Unsetenv("HTTP_PROXY")
	os.Setenv("http_proxy", FAKEPROXYURL_GOOD)
	viper.Set("ignore-proxy-config", false)

	endpointURL, parseError := url.ParseRequestURI(FAKEENDPOINT_HTTP)
	if parseError != nil {
		t.Fatalf("Unable to parse URI: %+v", parseError)
	}

	listener, listenError := net.Listen("tcp", FAKEENDPOINT_TCP)
	if listenError != nil {
		t.Fatalf("Unable to start fake server for testing: %+v", listenError)
	}

	go func() {
		defer listener.Close()
		_, acceptError := listener.Accept()
		if acceptError != nil {
			t.Errorf("Unexpected error accepting the connection: %+v", acceptError)
		}

		t.Log("Connection accepted")
	}()

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL == nil {
		t.Fatalf("Didn't expect proxy to be nil")
	}

	if proxyURL.String() != FAKEPROXYURL_GOOD {
		t.Fatalf("Expected proxy to be set to %+v, instead it was: %+v", FAKEPROXYURL_GOOD, proxyURL.String())
	}
}

func TestCanAccessEndpoint_configuration_file(t *testing.T) {
	os.Unsetenv("HTTP_PROXY")
	os.Unsetenv("http_proxy")
	viper.Set("ignore-proxy-config", false)

	httpProxyStringMap := make(map[string]interface{})

	httpProxyStringMap["FakeProxyConfiguration1"] = map[string]interface{}{}

	httpProxyStringMap["FakeProxyConfiguration2"] = map[string]interface{}{
		"ProxyURL": FAKEPROXYURL_UNPARSABLE,
		"NoProxy":  "nothing.local",
	}

	httpProxyStringMap["FakeProxyConfiguration3"] = map[string]interface{}{
		"ProxyURL": FAKEPROXYURL_GOOD,
		"NoProxy":  "nothing.local",
	}

	viper.Set("HTTPProxies", httpProxyStringMap)

	log.Debugf("HTTPProxies: %+v", viper.GetStringMap("HTTPProxies"))

	endpointURL, parseError := url.ParseRequestURI(FAKEENDPOINT_HTTP)
	if parseError != nil {
		t.Fatalf("Unable to parse URI: %+v", parseError)
	}

	listener, listenError := net.Listen("tcp", FAKEENDPOINT_TCP)
	if listenError != nil {
		t.Fatalf("Unable to start fake server for testing: %+v", listenError)
	}

	go func() {
		defer listener.Close()
		_, acceptError := listener.Accept()
		if acceptError != nil {
			t.Errorf("Unexpected error accepting the connection: %+v", acceptError)
		}

		t.Log("Connection accepted")
	}()

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL == nil {
		t.Fatalf("Didn't expect proxy to be nil")
	}

	if proxyURL.String() != FAKEPROXYURL_GOOD {
		t.Fatalf("Expected proxy to be set to %+v, instead it was: %+v", FAKEPROXYURL_GOOD, proxyURL.String())
	}
}

func TestCanAccessEndpoint_bad_configuration_file(t *testing.T) {
	os.Unsetenv("HTTP_PROXY")
	os.Unsetenv("http_proxy")
	viper.Set("ignore-proxy-config", false)

	httpProxyStringMap := make(map[string]interface{})

	httpProxyStringMap["FakeProxyConfiguration1"] = map[string]interface{}{}

	httpProxyStringMap["FakeProxyConfiguration2"] = map[string]interface{}{
		"ProxyURL": FAKEPROXYURL_UNPARSABLE,
		"NoProxy":  "nothing.local",
	}

	viper.Set("HTTPProxies", httpProxyStringMap)

	log.Debugf("HTTPProxies: %+v", viper.GetStringMap("HTTPProxies"))

	endpointURL, _ := url.Parse(FAKEENDPOINT_HTTP)

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL != nil {
		t.Fatalf("Expected proxy to be nil")
	}
}

func TestCanAccessEndpoint_unreachable_HTTP_PROXY(t *testing.T) {
	os.Setenv("HTTP_PROXY", FAKEPROXYURL_GOOD)
	os.Unsetenv("http_proxy")
	viper.Set("ignore-proxy-config", false)

	endpointURL, _ := url.Parse(FAKEENDPOINT_HTTP)

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL != nil {
		t.Fatalf("Expected proxy to be nil")
	}
}

func TestCanAccessEndpoint_unreachable_http_proxy(t *testing.T) {
	os.Unsetenv("HTTP_PROXY")
	os.Setenv("http_proxy", FAKEPROXYURL_GOOD)
	viper.Set("ignore-proxy-config", false)

	endpointURL, _ := url.Parse(FAKEENDPOINT_HTTP)

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL != nil {
		t.Fatalf("Expected proxy to be nil")
	}
}

func TestCanAccessEndpoint_UnparsableProxy(t *testing.T) {
	os.Setenv("HTTP_PROXY", FAKEPROXYURL_UNPARSABLE)
	os.Setenv("http_proxy", FAKEPROXYURL_UNPARSABLE)
	viper.Set("HTTPProxies", nil)

	endpointURL, _ := url.Parse(FAKEENDPOINT_HTTP)

	proxyURL, proxyError := http.DefaultTransport.(*http.Transport).Proxy(&http.Request{URL: endpointURL})
	if proxyError != nil {
		t.Fatalf("Unable to determine proxy: %+v", proxyError)
	}

	if proxyURL != nil {
		t.Fatalf("Expected proxy to be nil, instead it was: %+v", proxyURL.String())
	}
}
