package cmd // import "electric-it.io/cago/cmd"

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/apex/log"
	"github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"gopkg.in/AlecAivazis/survey.v1"

	"electric-it.io/cago/aws"
)

const (
	// The selected file will be written to this file for use by wrapper scripts
	profileOutputFileName = ".cago/cago.profile.txt"
)

// chooseProfileCmd represents the chooseProfile command
var chooseProfileCmd = &cobra.Command{
	Use:   "choose-profile",
	Short: "Choose from a list of Cago managed profiles and return the chosen profile to stdout",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		profileNames, getAllManagedProfileNamesError := aws.GetAllManagedProfileNames()
		if getAllManagedProfileNamesError != nil {
			log.Errorf("Error while retrieving profile names")
			return getAllManagedProfileNamesError
		}

		if len(profileNames) == 0 {
			log.Errorf("No managed profiles found! You probably should do a refresh.")
			return errors.New("No managed profiles found")
		}

		profile := ""
		prompt := &survey.Select{
			Message: "Choose a profile:",
			Options: profileNames,
		}
		askError := survey.AskOne(prompt, &profile, nil)
		if askError != nil {
			log.Errorf("Error in prompt: %s", askError)
			return askError
		}

		// Output the selected profile to a temporary file
		profileOutputFile, getProfileOutputFileError := getProfileOutputFile()
		if getProfileOutputFileError != nil {
			log.Errorf("Error getting profile output file: %s", getProfileOutputFileError)
			return getProfileOutputFileError
		}

		log.Debugf("Writing selected profile to: %s", profileOutputFile)
		writeError := ioutil.WriteFile(profileOutputFile, []byte(profile), 0644)
		if writeError != nil {
			log.Errorf("Unable to write profile to (%s): %s", profileOutputFile, writeError)
			return writeError
		}

		// This must go to stdout
		fmt.Println(profile)

		return nil
	},
}

func init() {
	rootCmd.AddCommand(chooseProfileCmd)
}

func getProfileOutputFile() (string, error) {
	homedirpath, homedirError := homedir.Dir()
	if homedirError != nil {
		log.Errorf("Unable to get user's home directory: %s", homedirError)
		return "", homedirError
	}

	// Calculate the location of the file to write the selected profile to
	return filepath.Join(homedirpath, profileOutputFileName), nil
}
