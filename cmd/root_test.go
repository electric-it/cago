package cmd_test // import "electric-it.io/cago/cmd"

import (
	"bytes"
	"os"
	"strings"
	"testing"

	"github.com/apex/log"
	"github.com/apex/log/handlers/text"

	"electric-it.io/cago/cmd"
)

func init() {
	log.SetHandler(text.New(os.Stdout))
}

func TestNoArguments(t *testing.T) {
	args := []string{""}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Error("Unexpected error when running command with no arguments")
	}

	if outputBuffer.String() == "" {
		t.Error("Expected some output when running command with no arguments")
	}
}

func TestBadArgument(t *testing.T) {
	args := []string{"nonsense"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Error("Expected error when running command with no arguments")
	}
}

func TestVersion(t *testing.T) {
	args := []string{"version"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Errorf("Unexpected error: %s", executeError)
	}

	log.Infof("%s", outputBuffer.String())

	if strings.TrimRight(outputBuffer.String(), "\n") != string(cmd.Version) {
		t.Errorf("Expected %s; got %s", cmd.Version, outputBuffer.String())
	}
}

func TestBadDebug(t *testing.T) {
	args := []string{"version", "-d=blah"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected error parsing bad flag")
	}

	if !strings.Contains(outputBuffer.String(), "invalid argument") {
		t.Errorf("Expected to find 'invalid argument' in output, but found this: \n===========================\n%s===========================\n", outputBuffer.String())
	}

}

func TestDebug(t *testing.T) {
	args := []string{"version", "-d"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError != nil {
		t.Errorf("Unexpected error: %s", executeError)
	}

	if !strings.Contains(outputBuffer.String(), "Debug logging enabled!") {
		t.Errorf("Expected to find 'Debug logging enabled!' in output, but found this: \n===========================\n%s===========================\n", outputBuffer.String())
	}
}

func TestConfigFileFlagNoFile(t *testing.T) {
	args := []string{"version", "-d", "-c"}
	outputBuffer := bytes.NewBufferString("")

	executeError := cmd.ExecuteForTesting(args, outputBuffer)
	if executeError == nil {
		t.Errorf("Expected error when using config file flag without config file")
	}

	log.Infof("%s", outputBuffer.String())

	if outputBuffer.String() == "" {
		t.Error("Expected some output when running command with no arguments")
	}
}
