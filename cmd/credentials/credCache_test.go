package credentials_test // import "electric-it.io/cago/cmd/credentials"

import (
	"testing"

	"github.com/zalando/go-keyring"

	"electric-it.io/cago/cmd/credentials"
)

func TestParseAWSRoles(t *testing.T) {
	keyring.MockInit()

	{
		password := credentials.GetCachedPassword()
		if password != "" {
			t.Errorf("Expected password to be empty")
		}
	}

	{
		username := credentials.GetCachedUsername()
		if username != "" {
			t.Errorf("Expected username to be empty")
		}
	}

	{
		credentials.CachePassword("password")

		password := credentials.GetCachedPassword()
		if password != "password" {
			t.Errorf("Expected password; got %s", password)
		}
	}

	{
		credentials.CacheUsername("username")

		username := credentials.GetCachedUsername()
		if username != "username" {
			t.Errorf("Expected username; got %s", username)
		}
	}
}
