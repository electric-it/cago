module electric-it.io/cago

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/Netflix/go-expect v0.0.0-20180928190340-9d1f4485533b // indirect
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/apex/log v1.1.0
	github.com/aws/aws-sdk-go v1.17.12
	github.com/beevik/etree v1.1.0
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/danieljoos/wincred v1.0.1 // indirect
	github.com/go-errors/errors v1.0.1
	github.com/go-ini/ini v1.42.0
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/golang/protobuf v1.3.0 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/hinshun/vt10x v0.0.0-20180809195222-d55458df857c // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/magefile/mage v1.8.0
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.6 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/smartystreets/assertions v0.0.0-20190215210624-980c5ac6f3ac // indirect
	github.com/smartystreets/goconvey v0.0.0-20190222223459-a17d461953aa // indirect
	github.com/spf13/afero v1.2.1 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.3.1
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/thoas/go-funk v0.0.0-20181020164546-fbae87fb5b5c
	github.com/xanzy/go-gitlab v0.16.0
	github.com/yosssi/gohtml v0.0.0-20190128141317-9b7db94d32d9
	github.com/zalando/go-keyring v0.0.0-20190208082241-fbe81aec3a07
	golang.org/x/crypto v0.0.0-20190228161510-8dd112bcdc25 // indirect
	golang.org/x/net v0.0.0-20190301231341-16b79f2e4e95
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421 // indirect
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
	golang.org/x/sys v0.0.0-20190306171555-70f529850638 // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
	gopkg.in/AlecAivazis/survey.v1 v1.8.2
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
	gopkg.in/jarcoal/httpmock.v1 v1.0.0-20190304095222-3b6b0a8dbc05
)
