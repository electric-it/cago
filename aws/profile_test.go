package aws_test // import "electric-it.io/cago/aws"

import (
	"sync"
	"testing"
	"time"

	"github.com/go-errors/errors"
	"github.com/spf13/viper"

	"electric-it.io/cago/aws"
)

func init() {
	viper.Set("TESTMODE", true)
}

func TestDoesProfileExist(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	aws.CredentialsFilePath = "testdata/good-credentials-file.ini"

	{
		profileExists, doesProfileExistError := aws.DoesProfileExist("NonExistentProfile")
		if doesProfileExistError != nil {
			t.Fatalf("Unable to check if profile exists\n%s", doesProfileExistError.(*errors.Error).ErrorStack())
		}

		if profileExists {
			t.Errorf("got %t; expected false", profileExists)
		}
	}
}

func TestAddProfile(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	{
		addProfileError := aws.AddProfile("NewProfile")
		if addProfileError != nil {
			t.Fatalf("Unable to add profile\n%s", addProfileError.(*errors.Error).ErrorStack())
		}
	}

	{
		addProfileError := aws.AddProfile("NewProfile")
		if addProfileError == nil {
			t.Fatalf("Expected error when trying to add profile that already exists")
		}
	}

	{
		profileExists, doesProfileExistError := aws.DoesProfileExist("NewProfile")
		if doesProfileExistError != nil {
			t.Fatalf("Unable to check if profile exists\n%s", doesProfileExistError.(*errors.Error).ErrorStack())
		}

		if !profileExists {
			t.Errorf("got %t; expected true", profileExists)
		}
	}
}

func TestDeleteProfile(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	{
		addProfileError := aws.AddProfile("NewProfile")
		if addProfileError != nil {
			t.Fatalf("Unable to add profile\n%s", addProfileError.(*errors.Error).ErrorStack())
		}
	}

	{
		deleteProfileError := aws.DeleteProfile("NewProfile")
		if deleteProfileError != nil {
			t.Fatalf("Unable to delete profile\n%s", deleteProfileError.(*errors.Error).ErrorStack())
		}
	}

	{
		deleteProfileError := aws.DeleteProfile("NewProfile")
		if deleteProfileError != nil {
			t.Fatalf("Unable to delete profile\n%s", deleteProfileError.(*errors.Error).ErrorStack())
		}
	}
}

func TestSetExpiration(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	aws.CredentialsFilePath = "testdata/good-credentials-file.ini"

	setExpirationError := aws.SetExpiration("NotCagoManaged", time.Now())
	if setExpirationError == nil {
		t.Fatalf("Expected an error when trying to set expiration on a non-Cago managed profile")
	}
}

func TestSetExpiration_Happy(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	{
		setExpirationError := aws.SetExpiration("NonExistentProfile", time.Now())
		if setExpirationError == nil {
			t.Fatalf("Expected an error when trying to set expiration on a non-existent profile")
		}
	}

	// Create a profile in the temporary file to play with
	addProfileError := aws.AddProfile("ProfileA")
	if addProfileError != nil {
		t.Fatalf("Unable to add profile\n%+v", addProfileError)
	}

	expirationTime := time.Now()
	setExpirationError := aws.SetExpiration("ProfileA", expirationTime)
	if setExpirationError != nil {
		t.Fatalf("Unable to check expiration\n%+v", setExpirationError)
	}
}

func TestIsExpired(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	{
		// Create a profile in the temporary file to play with
		addProfileError := aws.AddProfile("ExpiredProfile")
		if addProfileError != nil {
			t.Fatalf("Unable to add profile\n%+v", addProfileError)
		}

		setExpirationError := aws.SetExpiration("ExpiredProfile", time.Now().AddDate(0, 0, -1))
		if setExpirationError != nil {
			t.Fatalf("Unable to set expiration\n%+v", setExpirationError)
		}

		isExpired, isExpiredError := aws.IsExpired("ExpiredProfile")
		if isExpiredError != nil {
			t.Fatalf("Unable to check expiration: %s\n%s", isExpiredError, isExpiredError.(*errors.Error).ErrorStack())
		}

		if !isExpired {
			t.Errorf("got %t; expected true", isExpired)
		}
	}

	{
		// Create a profile in the temporary file to play with
		addProfileError := aws.AddProfile("NotExpiredProfile")
		if addProfileError != nil {
			t.Fatalf("Unable to add profile\n%+v", addProfileError)
		}

		setExpirationError := aws.SetExpiration("NotExpiredProfile", time.Now().AddDate(0, 0, 1))
		if setExpirationError != nil {
			t.Fatalf("Unable to set expiration\n%+v", setExpirationError)
		}

		isExpired, isExpiredError := aws.IsExpired("NotExpiredProfile")
		if isExpiredError != nil {
			t.Fatalf("Unable to check expiration: %s\n%v", isExpiredError, isExpiredError.(*errors.Error).ErrorStack())
		}

		if isExpired {
			t.Errorf("got %t; expected false", isExpired)
		}
	}

	{
		_, isExpiredError := aws.IsExpired("NotAProfile")
		if isExpiredError == nil {
			t.Fatalf("Expected an error when trying to check a non-existent profile")
		}
	}
}

func TestGetAllManagedProfileNames_BadCredentialsFile(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	{
		_, getAllManagedProfileNamesError := aws.GetAllManagedProfileNames()
		if getAllManagedProfileNamesError != nil {
			t.Fatalf("Unexpected error for GetAllManagedProfileNames() when there are no profiles")
		}
	}
}

func TestGetKeyValue_NonExistentProfile(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	_, getKeyValueError := aws.GetKeyValue("NonExistentProfile", "NonExistentKey")
	if getKeyValueError == nil {
		t.Fatalf("Expected error trying to get key value to a non-existent profile")
	}
}

func TestGetKeyValue_NonExistentKey(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	addProfileError := aws.AddProfile("ProfileA")
	if addProfileError != nil {
		t.Fatalf("Unable to add profile\n%+v", addProfileError)
	}

	keyValue, getKeyValueError := aws.GetKeyValue("ProfileA", "NonExistentKey")
	if getKeyValueError == nil {
		t.Fatalf("Expected error trying to get non-existent key value, instead got key value: %s", keyValue)
	}
}

func TestGetKeyValue_Happy(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	addProfileError := aws.AddProfile("ProfileA")
	if addProfileError != nil {
		t.Fatalf("Unable to add profile\n%+v", addProfileError)
	}

	keyValue, getKeyValueError := aws.GetKeyValue("ProfileA", "cago_managed")
	if getKeyValueError != nil {
		t.Fatalf("Unable to get key value")
	}

	if keyValue != "true" {
		t.Errorf("got %s names; expected %s names", keyValue, "true")
	}
}

func TestAddKeyValue(t *testing.T) {
	// Recreate the sync object to force the load to run again for the test
	aws.CredentialsFileLoadSync = sync.Once{}

	{
		addKeyValueError := aws.AddKeyValue("TestProfile", "test-key", "test-value")
		if addKeyValueError == nil {
			t.Fatalf("Expected error trying to add a key value to a non-existent profile")
		}
	}

	{
		addProfileError := aws.AddProfile("TestProfile")
		if addProfileError != nil {
			t.Fatalf("Unable to add test profile\n%+v", addProfileError)
		}

		addKeyValueError := aws.AddKeyValue("TestProfile", "test-key", "test-value")
		if addKeyValueError != nil {
			t.Fatalf("Unable to add key value\n%+v", addKeyValueError)
		}
	}

	{
		addKeyValueError := aws.AddKeyValue("TestProfile", "", "test-value")
		if addKeyValueError == nil {
			t.Fatalf("Expected error trying to add a key with no name")
		}
	}
}
